window.onload = init;
var XMLMainElement = null;
var licznik = 0;
var wybrany = 0;
var tmpCode;

let _suggestBoxField = document.getElementById("suggestBoxField");
let _wojewodztwo = document.getElementById("wojewodztwo");
let _tekst = document.getElementById('tekst');

function init() {

    _suggestBoxField.style.left = _wojewodztwo.offsetLeft + 'px';
    _suggestBoxField.style.top = _wojewodztwo.offsetTop + wojewodztwo.offsetHeight + 'px';

    _wojewodztwo.onkeyup = function (e) {
        showBox(e);
        checkKey(e);
    }
    suggestBox();
}

function ajaxInit() {
    var XHR = null;

    try {
        XHR = new XMLHttpRequest();
    } catch (e) {
        try {
            XHR = new ActiveXObject("Msxml2.XMLHTTP");

        } catch (e2) {
            try {
                XHR = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e3) {
                alert("Niestety Twoja przeglądarka nie obsługuje AJAXA");
            }
        }
    }
    return XHR;
}

function suggestBox() {
    var XHR = ajaxInit();
    if (XHR != null) {
        // true - asynchornicznie 
        XHR.open("GET", "../data/wojewodztwa.xml" + "?random=" + Math.random(), true);

        XHR.onreadystatechange = function () {
            if (XHR.readyState == 4) {

                if (XHR.status == 200) {

                    XMLMainElement = XHR.responseXML.documentElement;

                } else
                    alert("Wystąpił błąd " + XHR.status);
            }
        }

        XHR.send(null);
    }
}

function showBox(evt) {

    //dla wszystkich przeglądarek
    var evt = (evt) ? evt : window.event;
    //po dodaniu literki zaznaczenie wraca na sam początek
    if (evt.keyCode != 13 && evt.keyCode != 38 && evt.keyCode != 40 && evt.keyCode != 8) {
        licznik = 0;
    }

    if (XMLMainElement != null) {

        //zerowanie
        _suggestBoxField.style.visibility = 'hidden';
        _suggestBoxField.innerHTML = '';
        _wojewodztwo.className = ''

        var wojewodztwa = XMLMainElement.getElementsByTagName("Województwo");

        if (_wojewodztwo.value != '') {
            for (var i = 0; i < wojewodztwa.length; i++)
                if (wojewodztwa[i].getElementsByTagName("Nazwa")[0].firstChild.nodeValue.indexOf(document.getElementById("wojewodztwo").value.toLowerCase()) == 0) {

                    // var suggestBoxField = document.getElementById('suggestBoxField');

                    _suggestBoxField.style.visibility = 'visible';

                    var tmpDiv = document.createElement('div');
                    tmpDiv.className = 'podpowiedzi';

                    // najechanie myszką
                    tmpDiv.onmouseover = function () {
                        this.className = 'podpowiedziHO'
                    }

                    //zejście muszką
                    tmpDiv.onmouseout = function () {
                        this.className = 'podpowiedzi'
                    }

                    //wybór podpowiedzi na kliknięcie
                    tmpDiv.onclick = function () {
                        _suggestBoxField.value = this.innerHTML;
                        wybraniePodpowiedzi(this.innerHTML);
                    }

                    //pobranie i dodanie wartości do SuggestBoxField
                    tmpDiv.innerHTML = wojewodztwa[i].getElementsByTagName("Nazwa")[0].firstChild.nodeValue;
                    _suggestBoxField.appendChild(tmpDiv);
                }

            //brak podpowiedzi
            if (_suggestBoxField.childNodes.length == 0) {
                _wojewodztwo.className = 'error';
            }
        }
    }
}

function checkKey(evt) {

    //dla wszystkich przeglądarek
    var evt = (evt) ? evt : window.event;

    var iloscPodpowiedzi = document.getElementById('suggestBoxField').childNodes.length;

    if (licznik == 0) {
        licznik = iloscPodpowiedzi;
        wybrany = 0;
    }
    //kliknięcie strzałka w dół
    if (evt.keyCode == 40) {
        //przypadek gdy poprzednio był inny przycisk
        if (tmpCode == 'gora') {
            licznik++;
        }
        document.getElementById('suggestBoxField').childNodes[licznik % iloscPodpowiedzi].className = "podpowiedziHO"
        wybrany = licznik % iloscPodpowiedzi;
        licznik++;
        tmpCode = 'dol';
    } else if (evt.keyCode == 38) {
        //przypadek gdy poprzednio był inny przycisk
        if (tmpCode == 'dol') {
            licznik--;
        }
        licznik--;
        wybrany = licznik % iloscPodpowiedzi;
        document.getElementById('suggestBoxField').childNodes[licznik % iloscPodpowiedzi].className = "podpowiedziHO"
        tmpCode = 'gora';

    }
    //kliknięcie enter
    else if (evt.keyCode == 13) {
        document.getElementById('wojewodztwo').value = document.getElementById('suggestBoxField').childNodes[wybrany % iloscPodpowiedzi].firstChild.nodeValue;
        tmpCode = 'enter';
        wybraniePodpowiedzi(document.getElementById('suggestBoxField').childNodes[wybrany % iloscPodpowiedzi].firstChild.nodeValue);


    }
    //kliknięcie spacji
    else if (evt.keyCode == 8) {
        licznik = 0;
        wybrany = 0;
        tmpCode = 'backspace';
    }
}

function wybraniePodpowiedzi(wybranyRekord) {

    _tekst.innerHTML = '';
    _suggestBoxField.style.visibility = 'hidden';
    var wybraneWojewodztwo = null;

    for (var i = 0; i < XMLMainElement.getElementsByTagName("Województwo").length; i++) {

        if (XMLMainElement.getElementsByTagName("Nazwa")[i].firstChild.nodeValue == wybranyRekord) {
            wybraneWojewodztwo = XMLMainElement.getElementsByTagName("Nazwa")[i].parentNode;
            break;
        }
    }

    if (wybraneWojewodztwo != null) {
        var table = document.createElement('table');
        table.className = 'daneOWojewodztwie';
        var tableBody = document.createElement('tbody');

        for (var i = 0; i < wybraneWojewodztwo.childNodes.length; i++) {

            if (wybraneWojewodztwo.childNodes[i].nodeType == 1) {

                var row = document.createElement('tr');
                var cell = document.createElement('td');

                var header = document.createTextNode(wybraneWojewodztwo.childNodes[i].nodeName + ": ");
                cell.className = 'cellHeader';

                cell.appendChild(header);
                row.appendChild(cell);

                cell = document.createElement('td');
                var content = document.createTextNode(wybraneWojewodztwo.childNodes[i].firstChild.nodeValue);
                cell.appendChild(content);
                row.appendChild(cell);
                tableBody.appendChild(row);
            }
        }
        table.appendChild(tableBody);

        document.getElementById('tekst').appendChild(table);
    }
}